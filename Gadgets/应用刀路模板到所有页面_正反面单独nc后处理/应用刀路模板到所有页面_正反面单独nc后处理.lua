-- VECTRIC LUA SCRIPT
--version3.1.2
-- Copyright 2013 Vectric Ltd.

-- Gadgets are an entirely optional add-in to Vectric's core software products.
-- They are provided 'as-is', without any express or implied warranty, and you make use of them entirely at your own risk.
-- In no event will the author(s) or Vectric Ltd. be held liable for any damages arising from their use.

-- Permission is granted to anyone to use this software for any purpose,
-- including commercial applications, and to alter it and redistribute it freely,
-- subject to the following restrictions:

-- 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
-- 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
-- 3. This notice may not be removed or altered from any source distribution.

-- Additions by Adrian Matthews to save each sheets' toolpath
-- 老鱼增加区分板材正反面应用不同的nc后处理文件

require "strict"

g_version     = "1.0"
g_title       = "ApplyTemplateToAllSheets_fen-zf"
g_gadget_name = "ApplyTemplateToAllSheets_fen-zf"

g_default_window_width  = 800
g_default_window_height = 600
g_isBottom =false
g_abf_isBottom = false
post_BOTTOM=nil
g_showerr = true
g_options =
   {
   -- Template options
   templatePath        = "",
   -- Window options
   windowWidth         = g_default_window_width,
   windowHeight        = g_default_window_height,
   postOutputFolder    = "",
   autoSaveToolpaths   = true
   }
--[[  -------------- SaveDefaults --------------------------------------------------
|
|  Save gadget options to the registry
|  Parameters:
|     options           -- The set of options for the gadget
|
]]
function SaveDefaults(options)
   local registry = Registry(g_gadget_name)
   registry:SetString("templatePath",  options.templatePath)
   registry:SetString("postOutputFolder", options.postOutputFolder)
   -- Save options
   registry:SetInt("WindowWidth", options.windowWidth)
   registry:SetInt("WindowHeight", options.windowHeight)
   registry:SetBool("autoSaveToolpaths", options.autoSaveToolpaths)
end
--[[  -------------- LoadDefaults --------------------------------------------------
|
|  Load the gadget options from the registry
|  Parameters:
|     options           -- The set of options for the gadget
|
]]
function LoadDefaults(options)
   local registry = Registry(g_gadget_name)
   options.templatePath = registry:GetString("templatePath", options.templatePath)
   options.postOutputFolder = registry:GetString("postOutputFolder", options.postOutputFolder)
   local window_width  = registry:GetInt("WindowWidth",  options.windowWidth)
   local window_height = registry:GetInt("WindowHeight", options.windowHeight)
   -- Check the window dimensions
   if window_width < 0 then
      window_width = options.windowWidth
   end
   if window_height < 0 then
      window_height = options.windowHeight
   end
   options.windowWidth  = window_width
   options.windowHeight = window_height
   options.autoSaveToolpaths = registry:GetBool("autoSaveToolpaths", options.autoSaveToolpaths)
end
--[[  -------------- UpdateOptionsFromDialog --------------------------------------------------
|
|  Updates our options from the dialog
|  Parameters:
|     dialog            -- The dialog to update the options from
|     options           -- The set of options for the gadget
|
]]
function UpdateOptionsFromDialog(dialog, options)
   -- Update the window dimensions - this only works for dialogs with an OK button in VCP 7.012 and Aspire 4.012 and earlier
   options.windowWidth  = dialog.WindowWidth
   options.windowHeight = dialog.WindowHeight
   local template_path = dialog:GetLabelField("TemplateFileNameLabel")
   if template_path == "" then
      MessageBox("No template file selected!");
      return false
   end

   options.templatePath = template_path;

   g_options.autoSaveToolpaths = dialog:GetCheckBox("SaveToolpathsCheck")
   
   local postOutputFolder = dialog:GetTextField("PostOutputFolderEdit")
   if postOutputFolder == "" then
      MessageBox("No toolpath output folder set!");
      return false
   end

   options.postOutputFolder = postOutputFolder

   -- Update successful
   return true

end

--[[  ------------------------ DisplayDialog --------------------------------------------------
|
|  Display dialog we use to select toolpath template etc
|
]]
function DisplayDialog(script_path)

   local script_html = "file:" .. script_path .. "\\Apply_Template_To_All_Sheets.htm"
    local dialog = HTML_Dialog(false, script_html, g_options.windowWidth, g_options.windowHeight, g_title)

   -- Standard title and version fields
   dialog:AddLabelField("GadgetTitle", g_title)
   dialog:AddLabelField("GadgetVersion", g_version)

    -- Label used to display path to template file
    dialog:AddLabelField("TemplateFileNameLabel", g_options.templatePath);
    dialog:AddFilePicker(true, "ChooseTemplateFileButton", "TemplateFileNameLabel", false);

    -- Controls for toolpath output
    dialog:AddCheckBox("SaveToolpathsCheck", g_options.autoSaveToolpaths)
    dialog:AddTextField("PostOutputFolderEdit", g_options.postOutputFolder)
    dialog:AddDirectoryPicker("ChooseDirButton", "PostOutputFolderEdit", true)

    -- populate the drop down list with the list of posts in the program
    PopulatePostDropDownList(dialog, "PostNameSelector", "")

    -- Show the dialog
   if not dialog:ShowDialog() then
      return false
   end

   -- Update the options from this dialog
   UpdateOptionsFromDialog(dialog, g_options)

    -- Process our toolpaths
   ProcessTemplate(dialog)

   -- Save values as defaults for next time
   SaveDefaults(g_options);

   return true

end

--[[  ------------------------ OnLuaButton_ChooseTemplateFileButton --------------------------------------------------
|
|  Choose a toolpath template file
|
]]
function OnLuaButton_ChooseTemplateFileButton(dialog)

   local file_dialog = FileDialog()
   if not file_dialog:FileOpen(
                              "ToolpathTemplate",
                              g_options.templatePath,
                              "Toolpath Templates (*.ToolpathTemplate)|*.ToolpathTemplate|"
                              ) then
       MessageBox("No template to use selected")
       return false
    end

   g_options.templatePath = file_dialog.PathName
   dialog:UpdateLabelField("TemplateFileNameLabel", g_options.templatePath);

   return true
end

--[[  ------------------------ OnLuaButton_SaveToolpathsCheck --------------------------------------------------
|
|  Called when user toggles saveing toolpaths check - we pick up the value when we close form
|
]]
function OnLuaButton_SaveToolpathsCheck(dialog)
   return true
end
--[[  ------------------------ RenameToolpathsForSheet --------------------------------------------------
|
|  Renames all the toolpaths loaded from a template for the current sheet. Each toolpath is prefixed with "Sn-"
|  where 'n' = the sheet number. This function also returns the id of the toolpaths so they can be calculated
|
]]
function RenameToolpathsForSheet(sheet_index, sheet_toolpath_start_index, sheet_toolpath_ids, toolpath_manager)

   -- skip over any toolpaths from previous sheets
   local toolpath_index = 0
   local toolpath = nil

   local pos = toolpath_manager:GetHeadPosition()
   while (pos ~= nil) and (toolpath_index < sheet_toolpath_start_index) do
        toolpath, pos = toolpath_manager:GetNext(pos)
      toolpath_index = toolpath_index + 1
   end
   -- we should now be pointing at the first toolpath associated with template for this sheet ...
   while (pos ~= nil) do
      toolpath, pos = toolpath_manager:GetNext(pos)
      local new_name = "S" .. sheet_index .. "-" .. toolpath.Name
      toolpath.Name = new_name
      toolpath_manager:ToolpathModified(toolpath)
      table.insert(sheet_toolpath_ids, toolpath.Id)
   end

   return true

end


--[[  ------------------------ DoAllToolpathsUseSameTool --------------------------------------------------
|
| Check if all the toolpaths in the passed list use the same tool. If they do, we can save all the 
| toolpaths to the same file, regardles of whether the post supports toolchanging. If the toolpaths
| use different tools and the post does not support tool changing, the toolpaths will need to be saved individually
|
]]
function DoAllToolpathsUseSameTool(sheet_toolpath_ids, toolpath_manager)

   local tool = nil
	
	for i,toolpath_id in ipairs(sheet_toolpath_ids) do
		local pos = toolpath_manager:Find(toolpath_id)
		if pos == nil then
		   MessageBox("Failed to find toolpath " .. i )
		   return false
		end
		local toolpath = toolpath_manager:GetAt(pos)
		-- do we have an existing tool to check against?
		if tool == nil then
		   -- no this is first toolpath - save a copy of tool to check against
		   tool = toolpath.Tool
		else
		   -- check if this tool is compatible with previous tools
		   if not tool:IsCompatibleTool(toolpath.Tool) then
		      return false  -- we have a mixture of tools 
		   end 	  
		end
	end  
	  
   return true
end

--[[  ------------------------ CheckDifferentToolsHaveUniqueToolNumbers --------------------------------------------------
|
| Check if all the toolpaths in the passed list which use a different tool have a unique tool number
|
]]
function CheckDifferentToolsHaveUniqueToolNumbers(toolpath_ids, toolpath_manager)

   local tools = {}
   local tool_count = 0
	
	for i,toolpath_id in ipairs(toolpath_ids) do
		local pos = toolpath_manager:Find(toolpath_id)
		if pos == nil then
		   MessageBox("Failed to find toolpath " .. i )
		   return false
		end
		local toolpath = toolpath_manager:GetAt(pos)
		-- do we have one or more tools to check against?
		if tool_count > 1 then
		   -- Yes - check if this tool is compatible with previous tools
         for i = 1, tool_count do
            if not toolpath.Tool:IsCompatibleTool(tools[i]) then
               -- this is a different tool, check tools have different tool numbers ...
               if toolpath.Tool.ToolNumber == tools[i].ToolNumber then
                  -- Error! We have two toolpaths with tools with the same tool number but different geometry,
                  -- if thess toolpaths are to be saved with a toolchanger post processor this is invalid!
                  return false
               end
            end
         end           
		end
      -- add tool to our list 
  	   tool_count = tool_count + 1
      tools[tool_count] = toolpath.Tool
	end  
	  
   return true
end

--[[  ------------------------ SaveSingleToolpath --------------------------------------------------
|
| Check if all the toolpaths in the passed list use the same tool. If they do, we can save all the 
| toolpaths to the same file, regardles of whether the post supports toolchanging. If the toolpaths
| use different tools and the post does not support tool changing, the toolpaths will need to be saved individually
|
]]
function SaveSingleToolpath(toolpath, output_folder, post, sheet_msg_list)
   local toolpath_saver = ToolpathSaver();
   toolpath_saver:AddToolpath(toolpath)
   local toolpath_file = toolpath.Name .. "." .. post.Extension
   local output_path = output_folder .. "\\" .. toolpath_file
   sheet_msg_list = sheet_msg_list .. "<tr><td></td><td>" .. toolpath.Name .. "</td><td>"
   if toolpath_saver:SaveToolpaths(post, output_path, false) then
      sheet_msg_list = sheet_msg_list .. "&nbsp;&nbsp;&nbsp;SAVED"
   else
      sheet_msg_list = sheet_msg_list .. "&nbsp;&nbsp;&nbsp;SAVE FAILED"
   end
   sheet_msg_list = sheet_msg_list .. "</td></tr>"
   return sheet_msg_list
end
--获取文件名
function getFileName(str)
   local idx = 1
   local odx = 0 
   local str2 =str
   while(idx)
   do
      idx = str:find("\\")
      if(idx) then
         odx = idx
      else
      idx = odx
      break
      end 
      str = str:sub(idx+1, string.len(str))
   end
   if(odx) then
      --DisplayMessageBox(str)
      return str
   else
   return str
   end
end
--[[  ------------------------ LoadTemplateAndCalculateForSheet --------------------------------------------------
|
|  Load the passed toolpath template, and calculate its toolpaths for the passed sheet
|
]]
function LoadTemplateAndCalculateForSheet(post, dialog, sheet_index, toolpath_template, toolpath_manager, save_toolpaths, toolpath_saver, job, message_array)
   -- save the number of toolpaths already loaded - our toolpaths from template will be loaded after these
   local sheet_toolpath_start_index = toolpath_manager.Count
   -- load the toolpath template for this sheet
   if not toolpath_manager:LoadToolpathTemplate(toolpath_template) then
      MessageBox("Failed to load template " .. toolpath_template)
      return false
   end
   -- rename the toolpaths just loaded to have the sheet number as a prefix ...
   local sheet_toolpath_ids = {}
   RenameToolpathsForSheet(sheet_index, sheet_toolpath_start_index, sheet_toolpath_ids, toolpath_manager)
    -- Check if all the toolpaths use the same tool ...
   local all_toolpaths_use_same_tool = DoAllToolpathsUseSameTool(sheet_toolpath_ids, toolpath_manager)
   local can_save_to_single_file = all_toolpaths_use_same_tool or post.SupportsToolchange
   -- If we are saving the toolpaths AND we are usign a toolchanging post, check all toolpaths with
   -- different tool geometry are using different tool numbers - only check for first sheet ...
   if save_toolpaths and post.SupportsToolchange and sheet_index == 1 then
      if not CheckDifferentToolsHaveUniqueToolNumbers(sheet_toolpath_ids, toolpath_manager) then
         MessageBox("ERROR:\n" ..
                    "Some of the tools in the template have the\n" ..
                    "same tool number but different types/geometry.\n\n" ..
                    "To output the toolpaths using a tool changing post processor\n" ..
                    "each tool must have a unique tool number")
         return false           
      end
   end
   -- now calculate each toolpath ...
   local sheet_msg_list = ""
   for i,toolpath_id in ipairs(sheet_toolpath_ids) do
      local pos = toolpath_manager:Find(toolpath_id)
      if pos == nil then
         MessageBox("Failed to find toolpath " .. i .. " with id for sheet " .. sheet_index .. " after rename")
         return false
      end
      local toolpath = toolpath_manager:GetAt(pos)
      sheet_msg_list = sheet_msg_list .. "<tr><td></td><td>" .. toolpath.Name .. "</td><td>"
      -- when we recalculate a toolpath it is recreated but with the same id - save the id here
      -- so we can find the toolpath again after it is recalculated
      local orig_id = luaUUID()
      orig_id:SetId(toolpath_id)
      if toolpath_manager:RecalculateToolpath(toolpath) then
         sheet_msg_list = sheet_msg_list .. "&nbsp;&nbsp;&nbsp;Calculated O.K"
         -- MessageBox("Finding recalculated toolpath with same id - " .. orig_id:AsString())
         local calced_pos = toolpath_manager:Find(orig_id.RawId)
         if calced_pos == nil then
            MessageBox("Failed to find toolpath " .. i .. " with id for sheet " .. sheet_index .. " after calculation")
            return false
         else
            local toolpath = toolpath_manager:GetAt(calced_pos)
            if can_save_to_single_file then
               toolpath_saver:AddToolpath(toolpath)
            else
               -- this file needs to be saved with its own name as post doesnt support toolchanging
               if save_toolpaths then
                  sheet_msg_list = SaveSingleToolpath(toolpath, g_options.postOutputFolder, post, sheet_msg_list)
               end   
            end            
         end
      else
         sheet_msg_list = sheet_msg_list .. "&nbsp;&nbsp;&nbsp;FAILED"
         toolpath_manager:DeleteToolpathWithId(toolpath_id)
      end
      sheet_msg_list = sheet_msg_list .. "</td></tr>"
   end
   -- Save toolpaths for this sheet to the output folder, IF they all use the same tool or the post supported tool changing...
   if save_toolpaths and can_save_to_single_file then
      --sheet to dxf  读取dxf  文件
        local toolpath_file = "" --.. "." .. post.Extension
        local myfile = io.open("c:\\dxf_filelist2.txt", "r")
        local myIndex = 1 
        local rightNum = 1 --文件名行号
        local lineNum = 0
         if myfile ~= nil then 
          --取得文件名中sheet ID……是说sheetNum？然后好像没用来做什么啊……
          for l in myfile:lines() do
               local strLineContent =getFileName(l)
               strLineContent = string.gsub(strLineContent,".dxf","",1)
               local list = Split(strLineContent, "_")
               local kk = 0
               local sheetNum = 0
               local isTOP = false -- if has top bottom 
               local isBOTTOM = false -- if has top bottom
               g_abf_isBottom= false
               for i, v in ipairs(list) do
                  print(i, v)
                  if v == "SHEET" then
                   kk = i +1
                  end
                  if v == "TOP" then
                     isTOP = true
                  end
                  if v == "BOTTOM" then
                     isBOTTOM = true
                    --  g_abf_isBottom = true
                    --  MessageBox("g_abf_isBottom???".. tostring(g_abf_isBottom))
                  end
                  if(i == kk) then
                     sheetNum = tonumber(v)
                  end
               end 
               local inSheetNum = countMarkID(job) --asp sheet num 好像永远是返回0
               --TOP
               --sheetNum和inSheetNum会有相等的时候吗？
              --  MessageBox("sheetNum???"..sheetNum.. "inSheetNum???"..inSheetNum) 
              if(sheetNum == inSheetNum) then
                  -- MessageBox("sheetNum==inSheetNum")
                  rightNum = myIndex
                  if not isTOP and not isBOTTOM then
                     break
                  end
                  if isTOP or isBOTTOM then
                     if not g_isBottom and isTOP then
                        break
                     end
                     if  isBOTTOM and g_isBottom then 
                        break
                     end
                  end
               end
               myIndex = myIndex + 1
            end
         end
         myfile.close()
         --MessageBox("A ".. rightNum.."B "..countMarkID(job))
      --correct file name      
       local fileNameNc = getdxfFileName(job)
       if fileNameNc == "$error" then
         fileNameNc ="Sheet_".. sheet_index
        end 
      fileNameNc = string.gsub(fileNameNc,".dxf","",1)
      --amtf
      local postkk=post
      g_abf_isBottom=false
      -- MessageBox("带反字? ".. string.find(fileNameNc,"反") ~= nil)
      -- if (string.find(fileNameNc,"kk") ~= nil) then
        -- MessageBox(fileNameNc)
      -- end  
      -- if (string.find(fileNameNc,"BOTTOM") ~= nil or string.find(fileNameNc,"反") ~= nil) then
      if string.find(fileNameNc,"_01$") ~= nil  then --根据文件末尾判断板材正反面
        g_abf_isBottom=true
        -- MessageBox("带反字 ".. fileNameNc)
        if post_BOTTOM==nil then
          -- MessageBox("没有找到对应的反面post ".. output_path)
        else
          postkk=post_BOTTOM
        end
        fileNameNc = fileNameNc .."反"
      else
        fileNameNc = fileNameNc .."正"
      end  

      fileNameNc = fileNameNc .."."..post.Extension
      toolpath_file = fileNameNc
      local output_path = g_options.postOutputFolder .. "\\" .. fileNameNc
     -- MessageBox("sheet Name new ".. output_path)
      local myfile2 = io.open("c:\\nc_outpath.txt","a")
      if myfile2 ~= nil  then
         myfile2:write(output_path .."\n")
         myfile2.close()
      end
      local toolpath_count = toolpath_saver.NumberOfToolpaths
      sheet_msg_list = sheet_msg_list .. "<tr><td></td><td>" .. toolpath_file .. " (" .. toolpath_count .." toolpaths)</td><td>"
     -- 开始用到后处理保存刀路了？↓
      if toolpath_count > 0 then
          local success = toolpath_saver:SaveToolpaths(postkk, output_path, false)
           if success then
              sheet_msg_list = sheet_msg_list .. "&nbsp;&nbsp;&nbsp;SAVED OK"
           else
              sheet_msg_list = sheet_msg_list .. "&nbsp;&nbsp;&nbsp;FAILED"
           end
      else
         sheet_msg_list = sheet_msg_list .. "no toolpaths saved"
      end
      sheet_msg_list = sheet_msg_list .. "</td></tr>"
   end
   table.insert(message_array, sheet_msg_list)
   return true
end
--[[  ------------------------ OnLuaButton_ApplyButton --------------------------------------------------
|
|  Apply Button handler
|
]]
function OnLuaButton_ApplyButton(dialog)
   ProcessTemplate(dialog)
   return true
end
function file_existsA(path)
   local file = io.open(path, "rb")
   if file then file:close() end
   return file ~= nil
 end

 function SelectVectorsOnLayer(doC,Layer_Name,select_closed, select_open, select_groups)
    -- clear current selection
  local selection = doC.Selection
  local selection2 = GeometrySelector()
  selection2.OnlyOnLayers =true
  selection2:AddLayerhName(Layer_Name)
  
  local objects_selected = false
  local warning_displayed = false
  local layer = doC.LayerManager:FindLayerWithName(Layer_Name)
  if layer == nil then
          MessageBox("Layer name not found")
          return false
  end 
  local object = nil
  
  
  local sheetUUID =doC.LayerManager.ActiveSheetId
  
  local pos = layer:GetHeadPosition()
     while pos ~= nil do
        object, pos = layer:GetNext(pos)
        --local inSheetID = object.SheetId
        
        local Contour = object:GetContour()
      
        if Contour == nil then
           if (object.ClassName == "vcCadObjectGroup") and select_groups  and object.IsSelected then
            selection2:Add(object,true,true)
              objects_selected = true
           else 
              if not warning_displayed then
                 local message = "Object(s) without contour information found on layer - ignoring"
                 if not select_groups then
                    message = message .. 
                              "\r\n\r\n" .. 
                              "If layer contains grouped vectors these must be ungrouped for this script"
                 end
                 DisplayMessageBox(message)
                 warning_displayed = true
              end   
           end
        else  -- contour was NOT nill, test if Open or Closed
           if Contour.IsOpen and select_open  and object.IsSelected then
            
            selection2:Add(object,true,true)
              objects_selected = true
             --MessageBox("ss")
           else if select_closed and object.IsSelected then
            selection2:Add(object,true,true)
              objects_selected = true
           end            
        end
        end
     end  
  -- to avoid excessive redrawing etc  we added vectors to the selection in 'batch' mode
  -- tell selection we have now finished updating
 
  if(objects_selected) then
   
     selection:GroupSelectionFinished()
     selection:Clear()
     selection.Add(selection2,true,true)
  end  

  return objects_selected   
end  
function toint(n) 
   local s = tostring(n) 
   local i, j = s:find('%.') 
   if i then 
    return tonumber(s:sub(1, i-1)) 
   else 
    return n 
   end 
end 
--[[  ------------------------ ProcessTemplate --------------------------------------------------
|
|  Process the toolpath template for each sheet
|
]]
function countMarkID(job)
   job:SelectAllVectors()
   --读取标记
   local layer = job.LayerManager:FindLayerWithName("LAYER0")
   if layer == nil then
        --  MessageBox("Layer name：LAYER0  not found")找这个干嘛？
         return 0
   end 
   local objectCount = 0
   local object 
   g_isBottom = false
   local pos = layer:GetHeadPosition()
   while pos ~= nil do
      object, pos = layer:GetNext(pos)
      --local inSheetID = object.SheetId
         local Contour = object:GetContour()
         if Contour ~= nil then
           --MessageBox("len  ".. toint(Contour.Length))
            if object.IsSelected and  Contour.IsClosed and toint(Contour.Length) == 6 then      
               objectCount = objectCount + 1
            end
            if object.IsSelected and Contour.IsOpen  and toint(Contour.Length) == 2 then
               g_isBottom = true
                --objectCount = objectCount -1
            end
         end
   end           
   objectCount= objectCount -1
  --  MessageBox("sheetID len "..objectCount.."g_isBottom? "..tostring(g_isBottom))
   return objectCount
end
function getdxfFileName(job)
   --sheet to dxf  读取dxf  文件
     local toolpath_file = "" --.. "." .. post.Extension
     local myfile = io.open("c:\\dxf_filelist2.txt", "r")
     local myIndex = 1 
     local rightNum = 0 --文件名行号
     local lineNum = 0
      if myfile ~= nil then 
         for l in myfile:lines() do
            --取得文件名中sheet ID
            local strLineContent =getFileName(l)
            strLineContent = string.gsub(strLineContent,".dxf","",1)
            local list = Split(strLineContent, "_")
            local kk = 0
            local sheetNum = 0
            local isTOP = false -- if has top bottom 
            local isBOTTOM = false -- if has top bottom 
            for i, v in ipairs(list) do
               print(i, v)
               if v == "SHEET" then
                kk = i +1
               end
               if v == "TOP" then
                  isTOP = true
               end
               if v == "BOTTOM" then
                  isBOTTOM = true
               end
               if(i == kk) then
                  sheetNum = tonumber(v)
               end
            end 
            local inSheetNum = countMarkID(job) --asp sheet num 
            -- MessageBox("countMarkID youzhima?  "..inSheetNum)
            --TOP
           if(sheetNum == inSheetNum) then
               rightNum = myIndex
               if not isTOP and not isBOTTOM then
                  break
               end
               if isTOP or isBOTTOM then
                  if not g_isBottom and isTOP then
                     break
                  end
                  if  isBOTTOM and g_isBottom then 
                     break
                  end
               end
            end
            myIndex = myIndex + 1
         end
      end
      myfile.close()
      --MessageBox("A ".. rightNum.."B "..countMarkID(job))
      local rightc = 0
      local selection0 = job.Selection
         if selection0 ~= nil then
               local pos = selection0:GetHeadPosition() local object
               while pos ~= nil do
                object, pos = selection0:GetNext(pos) 
                local strt = object:GetInt("fileName",0,true)
                rightc = strt
                --MessageBox(tostring(strt))
                break
               end
         end
         if rightc == 0 and rightNum ==0 then
            if g_showerr then
               MessageBox("error")
               g_showerr = false
            end
            return "$error"
         end
      if rightc == 0 then
         if g_showerr then
            MessageBox("please import with dxf process v3.1 ")
            g_showerr = false
         end
         g_showerr = false
      end
      myfile = io.open("c:\\dxf_filelist2.txt", "r")
      if rightNum ==0 or  rightc ~= 0  then
         if g_showerr then
            MessageBox("replace with log name,please import with dxf process v3.1")
            g_showerr = false
         end
         rightNum = rightc
      end
     if myfile ~= nil then
      myIndex =1 
        for l in myfile:lines() do 
         if myIndex == rightNum  then
            print(l)
            l = getFileName(l)
            l=string.gsub(l,".dxf","",1)
            --生成sheet
            --toolpath_file = toolpath_file .. l .. "." .. post.Extension
            toolpath_file = l 
         end
         myIndex = myIndex + 1
        end
     end
     myfile.close()
  return toolpath_file
end
function Split(szFullString, szSeparator)
   local nFindStartIndex = 1
   local nSplitIndex = 1
   local nSplitArray = {}
   while true do
      local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
      if not nFindLastIndex then
       nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
       break
      end
      nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
      nFindStartIndex = nFindLastIndex + string.len(szSeparator)
      nSplitIndex = nSplitIndex + 1
   end
   return nSplitArray
end
function ProcessTemplate(dialog)
   -- Get the current job
   local job = VectricJob()
   if not job.Exists then
      return true
   end
   -- Update the options from the dialog
   if not UpdateOptionsFromDialog(dialog, g_options) then
      return true
   end
   -- Create our toolpaths ...
   local toolpath_manager = ToolpathManager()
    -- get initial toolpath count - we will ignore these ...
   local initial_toolpath_count = toolpath_manager.Count
   local layer_manager = job.LayerManager
   -- get sheet count - note sheet 0 the default sheet counts as one sheet
   local num_sheets = layer_manager.NumberOfSheets
   -- get current active sheet index
   local orig_active_sheet_index = layer_manager.ActiveSheetIndex
   -- we need 1 or more sheets for this gadget .... 一个也可以?
   if num_sheets < 2 then
      MessageBox("This gadget only works with 1 or more sheets")
      return false
   end
   -- get post processor to use for saving ...
   local post_name = dialog:GetDropDownListValue("PostNameSelector");
   local post_BOTTOM_name=string.sub(post_name, 1, -7) .."_BOTTOM(*.nc)"
  --  MessageBox("post_BOTTOM name " .. post_BOTTOM_name)
   local toolpath_saver = ToolpathSaver();
   local post = toolpath_saver:GetPostWithName(post_name)
   if post == nil then
      MessageBox("Failed to load Post Processor with name " .. post_name)
      return false
   end
   post_BOTTOM = toolpath_saver:GetPostWithName(post_BOTTOM_name)

   local sheet_index
   local message_array = {}
   local strTempsvg ="mkdir ".. g_options.postOutputFolder .. "\\svg\\"
   if file_existsA(strTempsvg) == false then
      os.execute(strTempsvg) --windows
   end
   --write svg
   local sheetIdLayer = job.LayerManager:GetLayerWithName("_ABF_SHEET_ID")
            local uuidSheetID = sheetIdLayer.Id
   for sheet_index = 1, num_sheets - 1 do
      layer_manager.ActiveSheetIndex = sheet_index
      job:Refresh2DView()
      local output_path = ""
       output_path =g_options.postOutputFolder .. "\\svg\\" .. getdxfFileName(job) ..".dxf.svg"
         --保存svg 开始选择
         job:SelectAllVectors()
         --local selRe =  SelectVectorsOnLayer(job,"_ABF_SHEET_ID",true,true,true)
         --获取 图层uuid
         --job.Selection:AddTail(myselA)
         --end 选择
         job:ExportSelectionToSvg(output_path)
            --保存svg ID only
            local pos = layer_manager:GetHeadPosition() 
            local layer
               while pos ~= nil do
                   layer, pos = layer_manager:GetNext(pos)
                   if layer.Id ~= uuidSheetID and layer.Visible then
                      layer.Visible = false
                   end
                   --DO SOMETHING WITH LAYER ....
               end
               job:SelectAllVectors()
               local selVector = job.Selection
               if selVector == nil then
                   MessageBox("没有选中图元")
               end
               job:ExportSelectionToSvg(output_path..".svg")
               local pos = layer_manager:GetHeadPosition() 
               local layer 
                 while pos ~= nil do
                        layer, pos = layer_manager:GetNext(pos)
                        layer.Visible = true
                  end            
   end
   for sheet_index = 1, num_sheets - 1 do
      layer_manager.ActiveSheetIndex = sheet_index
      job:Refresh2DView();
      toolpath_saver:ClearToolpathList()
      -- load the toolpath template for this sheet
      if not LoadTemplateAndCalculateForSheet(
									         post,
                                    dialog,
                                    sheet_index,
                                    g_options.templatePath,
                                    toolpath_manager,
                                    g_options.autoSaveToolpaths,
                                    toolpath_saver,
                                    job,
                                    message_array,
                                    toolpath_saver
									         ) then
         MessageBox("Failed to load toolpath template for sheet " .. sheet_index)
         break
      end

   end

   -- display messages ...
   DisplayStatusReport(message_array, job, g_options.templatePath)

   -- set active sheet to last sheet

   layer_manager.ActiveSheetIndex = orig_active_sheet_index

   job:Refresh2DView();



   return true

end

--[[  ------------------------ DisplayStatusReport --------------------------------------------------
|
|  Display a status report for toolpaths on each sheet
|
]]
function DisplayStatusReport(message_array, job, template_path)

  local page_html = g_ReportHeaderHtml

   page_html = page_html .. "<p><b>Job: </b>" .. job.Name .. "</p>"
   page_html = page_html .. "<p><b>Template: </b>" .. template_path .. "</p>"

   page_html = page_html .. "<table>"

   for i,sheet_msgs in ipairs(message_array) do
      page_html = page_html .. "<tr><td colspan=3><b>Sheet " .. i .. "</b></td></tr>"
      page_html = page_html .. sheet_msgs
      page_html = page_html .. "</p>"
   end
   page_html = page_html .. "</table>"

   page_html = page_html .. g_ReportFooterHtml

   local dialog = HTML_Dialog(true, page_html, g_options.windowWidth, g_options.windowHeight, g_title .. " - Status Report")

   dialog:ShowDialog()

   return true

end

--[[  --- PopulatePostDropDownList --------------------------------------------------
|
| Populate a drop down list selector with the list of post processors
|
|  Parameters:
|     dialog               - Dialog ofr post-processor list
|     drop_down_html_id    - id of html element for drop down list selector
|     default_post_name    - name of default pos - if nil use default from program

]]
function PopulatePostDropDownList(dialog, drop_down_html_id, default_post)

    -- Create object used to access post processors (and save toolpaths)
   local toolpath_saver = ToolpathSaver()

   -- if user hasn't supplied a default post name use the programs ...
   if (default_post == nil) or (default_post == "") then
      local default_pp = toolpath_saver.DefaultPost
      if default_pp ~= nil then
          default_post = default_pp.Name
      else
         default_post = ""
      end
    end

   -- link up to the <select> control on the dialog
   dialog:AddDropDownList(drop_down_html_id,default_post)

   -- and add our posts as options
   local num_posts = toolpath_saver:GetNumPosts()
   local post_index = 0

   while post_index < num_posts do
      local post = toolpath_saver:GetPostAtIndex(post_index)
      dialog:AddDropDownListValue(drop_down_html_id, post.Name)
      post_index = post_index + 1
   end

end


--[[  ------------------------ main --------------------------------------------------
|
|  Entry point for script
|
]]
function main(script_path)
    -- Check we have a job loaded
    local job = VectricJob()

    if not job.Exists then
       DisplayMessageBox("No job open.")
       return false
    end
    local myfile2 = io.open("c:\\nc_outpath.txt","w+")
    if myfile2 ~= nil  then
      
       myfile2.close()
    end
   -- Load our default values from the registry
   LoadDefaults(g_options);

   -- Display the dialog
   DisplayDialog(script_path)

   return true
end

g_ReportHeaderHtml = [[
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">

<style type="text/css">
html {
   overflow: auto;
}
body {
   background-color: #F0F0F0;
}
body, td, th {
   font-family: Arial, Helvetica, sans-serif;
   font-size: 12px;
}
.FormButton {
   font-weight: bold;
   width: 100%;
   font-family: Arial, Helvetica, sans-serif;
   font-size: 12px;
}
.h1 {
   font-size: 14px;
   font-weight: bold;
}
.h2 {
   font-size: 10px;
   font-weight: bold;
}
.ToolNameLabel {
   color: #555;
}

.ParameterDescription {
   color: #555;
}
.Warning {
   color: #955;
   font-size: 20px;
   font-weight: bold;
   text-align:center
}

</style>
<title>Vectric Gadget</title>
</head>
<body bgcolor="#efefef">
]]

g_ReportFooterHtml = [[
<table border="0" width="100%">
   <tr>
      <td style="width: 40%">&nbsp;</td>
      <td align = "center"><input id="ButtonCancel" class="FormButton" name="ButtonCancel" type="button" value="Close"></td>
      <td style="width: 40%">&nbsp;</td>
   </tr>
</table>
</body>
</html>
]]

