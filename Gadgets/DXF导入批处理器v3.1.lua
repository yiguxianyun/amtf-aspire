-- VECTRIC LUA SCRIPT
--version:3.1.1
--[[ =============================================================
|
|  Import all the DXF files in a directory and lay them out so that they
| dont overlap. This allows nesting to be easily done for a group of DXF files
|
| BrianM 01/02/2011 Remove output of log file to c:\temp\lua_filelist.txt
| BrianM 11/02/2011 Properley remove output of log file to c:\temp\lua_filelist.txt
| BrianM 15/01/2013 Update with 'strict' setting
| BrianM 19/02/2013 Update for 'VectricJob' use
|
| ===============================================================
]]

require "strict"


-- -----------  要处理的目录和文件过滤器 ------------------------------
g_base_directory = ""
g_file_filter = "*.dxf"
g_process_sub_dirs = true

-- ------------ 日志文件数据 ----------------------------------------------
g_output_log_file = true
g_log_file_name = "dxf_filelist.txt"
g_log_file_path = ""
g_log_file = nil
g_version  = "3.1"
--rename file

g_output_log_file2 = true
g_log_file_name2 = "rename.bat"
g_log_file_path2 = ""
g_log_file2 = nil

-- dxf file
g_output_log_file3 = true
g_log_file_name3 = ""
g_log_file_path3 = "c:\\dxf_filelist2.txt"
g_log_file3 = nil


-- ---------------- 用于布置导入向量的变量 -------------------

g_max_items_on_row  = 1 
g_border_gap_x = 0
g_border_gap_y = 5


g_sheet_indexi = 1

g_cur_row_bounds = Box2D()
g_cur_row_index = 1
g_cur_col_index = 0

--  --------------- 默认作业的设置 ----------------------------

g_default_job_name = "DXF Batch Layout"
g_default_job_width = 1220
g_default_job_height = 2440
g_default_job_thickness = 18
g_default_job_in_mm = false
g_default_job_origin = "BLC"
g_default_z_on_surface = true

g_job_bounds = Box2D()


--[[ ----------- 目录处理器 -------------------------------------------------
|
| Given a root directory and file filter, call the passed function with every file which matches the filter
|
| The do_sub_dirs flag indicates if sub-directories should be processed as well as the root directory
|
]]
function DirectoryProcessor(job, dir_name, filter, do_sub_dirs, function_ptr)

    local num_files_processed = 0;
    
    local directory_reader = DirectoryReader()
    local cur_dir_reader = DirectoryReader()

    directory_reader:BuildDirectoryList(dir_name, do_sub_dirs)
    directory_reader:SortDirs()

    local number_of_directories = directory_reader:NumberOfDirs()
    local progressBar = ProgressBar("导入dxf：",ProgressBar.LINEAR )
    for i = 1, number_of_directories do

        local cur_directory = directory_reader:DirAtIndex(i)
        
        -- get contents of current directory - dont include sub-dirs, use passed filter
        cur_dir_reader:BuildDirectoryList(cur_directory.Name, false)
        cur_dir_reader:GetFiles(filter, true, false)
        
        -- call passed method for each file ...
        local num_files_in_dir = cur_dir_reader:NumberOfFiles()
        --sort files
        cur_dir_reader:SortFiles(DirectoryReader.SORT_ALPHA, false)  --排序算法
        --cur_dir_reader:SortFiles(DirectoryReader.SORT_SIZE, false)
        for j=1, num_files_in_dir  do
           local file_info = cur_dir_reader:FileAtIndex(j)
           if not function_ptr(job, file_info.Name) then
              return -1
           end   
           num_files_processed = num_files_processed + 1
           progressBar:SetPercentProgress((num_files_processed/num_files_in_dir)*100)   
        end   
        
        -- empty out our directory object ready for next go 
        cur_dir_reader:ClearDirs()
        cur_dir_reader:ClearFiles()
        
    end    
    progressBar:Finished()
    return num_files_processed
end

--[[ -----------创建工作-------------------------------------------------
|
| Create a new empty job with the passed settings
|
]]
function CreateJob(
                  job_name, 
                  width, height, thickness, 
                  in_mm, 
                  job_origin, 
                  z_on_surface
                  )

   -- we fill in most of our bounds in a Box2D
   local job_bounds = Box2D()
   local blc = Point2D(0, 0)
   local trc = Point2D(width, height)
   
   -- claculate bottom left corner offset for chosen origin
   local origin_offset = Vector2D(0,0)
   if (job_origin == "BLC") then
      origin_offset:Set(0, 0)
   elseif (job_origin == "BRC") then
      origin_offset:Set(width, 0)
   elseif (job_origin == "TRC") then
      origin_offset:Set(width, height)
   elseif (job_origin == "TLC") then
      origin_offset:Set(0, height)
   elseif (job_origin == "CENTRE") then
      origin_offset:Set(width / 2, height / 2)
   elseif (job_origin == "CENTER") then
      origin_offset:Set(width / 2, height / 2)
   else
      MessageBox("指定了未知的XY原点 " .. job_origin)
   end
   
   -- subtract the origin offset vector from our 'standard' corner positions to get position for corners for requested origin
   blc = blc - origin_offset;
   trc = trc - origin_offset;
   
   job_bounds:Merge(blc)
   job_bounds:Merge(trc)
 
   local success = CreateNewJob(
                                job_name,
                                job_bounds,
                                thickness,
                                in_mm,
                                z_on_surface
                                )
                    
  return success

end



--[[ -----------导入Dxf文件-------------------------------------------------
|
| Import and then layout each file
|
| Write a succes / failure report to the output file
|
]]
--[[
function ImportDxfFile(job, filename)
   if g_output_log_file then
      g_log_file:write("输入  " .. filename .. "\n")
   end 
   if job:ImportDxfDwg(filename) then
      if g_output_log_file then
         g_log_file:write("成功导入\n")
	  end 
      LayoutImportedVectors(job)
   else
      if g_output_log_file then
         g_log_file:write("    导入失败\n")
      end		 
   end
    
   return true
end
]]

--获取文件名
function getFileName(str)
         local idx = 1
         local odx = 0 
         local str2 =str
         while(idx)
         do
            idx = str:find("\\")
            if(idx) then
               odx = idx
            else
            idx = odx
            break
            end 
            str = str:sub(idx+1, string.len(str))
         end
         if(odx) then
            --DisplayMessageBox(str)
            return str
         else
         return str
         end
end
   --获取扩展名
   function getExtension(str)
   return str:match(".+%.(%w+)$")
   end
--导入dxf  文件
function ImportDxfFile(job, filename)
   if g_output_log_file then
      g_log_file:write("输入  " .. filename .. "\n")
   end 
   if job:ImportDxfDwg(filename) then
      if g_output_log_file then
         g_log_file:write("成功导入\n")
         --已经导入的文件列表
         g_log_file2:write("ren Sheet"..g_sheet_indexi..".nc "..getFileName(filename) .. ".nc\n")
         g_log_file3:write(filename.."\n")
        -- local myImportBMP= CastCadObjectToCadBitmap(job.selection)
         g_sheet_indexi = g_sheet_indexi +1
	  end 
      LayoutImportedVectors(job , filename)
   else
      if g_output_log_file then
         g_log_file:write("    导入失败\n")
      end		 
   end
   return true
end

function Split(szFullString, szSeparator)
local nFindStartIndex = 1
local nSplitIndex = 1
local nSplitArray = {}
while true do
   local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
   if not nFindLastIndex then
    nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
    break
   end
   nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
   nFindStartIndex = nFindLastIndex + string.len(szSeparator)
   nSplitIndex = nSplitIndex + 1
end
return nSplitArray
end
function file_existsA(path)
   local file = io.open(path, "rb")
   if file then file:close() end
   return file ~= nil
 end
--导入dxf
function LayoutImportedVectors(job,fileNameA)

   -- get selected vectors bounding box 
      local selection = job.Selection
      if selection.IsEmpty then
         MessageBox("LayoutImportedVectors: No vectors selected!")
         return false
      end   
      --写入svg
      local strReplaceSource = Split(fileNameA,"\\")
      local count=0
        for k,v in pairs(strReplaceSource) do
	        count = count + 1
        end
        local strTempsvg = g_base_directory .. "\\svg\\"
      local strToRep = strReplaceSource[count]
       fileNameA =  strTempsvg .. strToRep ..".svg"
       --MessageBox(fileNameA)
      job:ExportSelectionToSvg(fileNameA)
      if selection ~= nil then
         local pos = selection:GetHeadPosition() local object
         while pos ~= nil do
          object, pos = selection:GetNext(pos) 
          object:SetInt("fileName", g_sheet_indexi-1)
         end
      end
      -- get bounding box of selection (imported vectors)
      local sel_bounds = selection:GetBoundingBox();
      -- calculate position for blc of imported vectors
      -- are we starting a new row?
      local blc_pos = Point2D()
      
      if g_cur_col_index >=  g_max_items_on_row then
         -- we will move up to start above currently imported data
         blc_pos = g_cur_row_bounds.TLC
         blc_pos.y = blc_pos.y + g_border_gap_y
         -- now reset bounds for this row
         g_cur_row_bounds:SetInvalid()
         g_cur_row_bounds:Merge(blc_pos);
         g_cur_row_index = g_cur_row_index + 1;
         g_cur_col_index = 1
      else
         -- we are going to place these vectors to the right of the last set (or the start of the row)
         blc_pos = g_cur_row_bounds.BRC
         blc_pos.x = blc_pos.x + g_border_gap_x
         g_cur_col_index = g_cur_col_index + 1
      end
      
      -- vector to transform blc of selection to required position
      local to_blc = blc_pos - sel_bounds.BLC
      local xform = TranslationMatrix2D(to_blc)
     
      selection:Transform(xform)
      
      -- get updated bounds for selection
      sel_bounds = selection:GetBoundingBox();
      -- and merge them into bounds for the current row
      g_cur_row_bounds:Merge(sel_bounds)
   end

--[[ ----------- 获取用户选择 -------------------------------------
|
| Display a dialog prompting user for options for processing
|
--]]
function GetUserChoices(job, script_path)
   -- get our default values from the registry (values used last time we were run)
   local registry = Registry("DxfFileProcessor")
   g_base_directory   = registry:GetString("BaseDirectory",  g_base_directory)
   g_process_sub_dirs = registry:GetBool  ("ProcessSubDirs", g_process_sub_dirs)
   g_output_log_file  = registry:GetBool  ("OutputLogFile",  g_output_log_file)
   g_log_file_path    = g_base_directory .. "\\" .. g_log_file_name

   -- ----------------用于布置导入向量的变量 -------------------
   g_max_items_on_row = registry:GetInt   ("MaxItemsOnRow",      g_max_items_on_row)
   g_border_gap_x     = registry:GetDouble("DefaultBorderGapX",  g_border_gap_x)
   g_border_gap_y     = registry:GetDouble("DefaultBorderGapY",  g_border_gap_y)

   --  --------------- 默认作业的设置 ----------------------------
   g_default_job_width     = registry:GetDouble("DefaultJobWidth",      g_default_job_width)
   g_default_job_height    = registry:GetDouble("DefaultJobHeight",     g_default_job_height)
   g_default_job_thickness = registry:GetDouble("DefaultJobThickness",  g_default_job_thickness)
   g_default_job_in_mm     = registry:GetBool  ("DefaultJobInMM",       g_default_job_in_mm)
   g_default_job_origin    = registry:GetString("DefaultJobOrigin",     g_default_job_origin)
   g_default_z_on_surface  = registry:GetBool  ("DefaultJobZOnSurface", g_default_z_on_surface)
   -- If we have a job open use units from that .....
   local in_mm = g_default_job_in_mm
   if job.Exists then
      in_mm = job.InMM 
   end
   -- display our dialog to get user choices
   local html_path = "file:" .. script_path .. "\\DXF_Batch_Processor.htm"
   local dialog = HTML_Dialog(true, g_DialogHtml, 680, 700, "DXF批处理器")
   local units_text = "mm"
   if g_default_job_in_mm then
      units_text = "mm"
   end   

   -- set the labels we display for units
   dialog:AddLabelField("Units1", units_text)
   dialog:AddLabelField("Units2", units_text)
   dialog:AddLabelField("Units3", units_text)
   dialog:AddLabelField("Units4", units_text)
   dialog:AddLabelField("Units5", units_text)
    
   --  Directory to process
   dialog:AddTextField("DirNameEdit", g_base_directory);
   dialog:AddDirectoryPicker("DirChooseButton", "DirNameEdit", true);
   dialog:AddCheckBox("ProcessSubDirsCheck", g_process_sub_dirs)

   --  Output log file
   dialog:AddCheckBox("CreateLogFileCheck", g_output_log_file )
   
   -- Layout Control 
   dialog:AddIntegerField("NumColumns", g_max_items_on_row)
   dialog:AddDoubleField("BorderGapX", g_border_gap_x)
   dialog:AddDoubleField("BorderGapY", g_border_gap_y)
    
   -- Drawing Dimensions - only used if no drawing currently open ...
   dialog:AddDoubleField("DrawingWidth", g_default_job_width)
   dialog:AddDoubleField("DrawingHeight", g_default_job_height)
   dialog:AddDoubleField("DrawingThickness", g_default_job_thickness)
    
   local units_index = 1;
   if (in_mm) then
      units_index = 2;
   end   
   dialog:AddRadioGroup("DrawingUnitsGroup", units_index)   
    -- XY origin
   local xy_origin_index = 1
   if g_default_job_origin == "TLC" then
      xy_origin_index = 1
   elseif g_default_job_origin == "TRC" then   
      xy_origin_index = 2
   elseif g_default_job_origin == "CENTER" then   
      xy_origin_index = 3
   elseif g_default_job_origin == "CENTRE" then   
      xy_origin_index = 3
   elseif g_default_job_origin == "BLC" then   
      xy_origin_index = 4
   elseif g_default_job_origin == "BRC" then   
      xy_origin_index = 5
   else
      MessageBox("对话框的XY未知原点 " .. g_default_job_origin)
   end
   dialog:AddRadioGroup("DrawingOrigin", xy_origin_index) 
    
   -- z origin
   local z_origin_index = 1;
   if not g_default_z_on_surface then
      z_origin_index = 2
   end
   dialog:AddRadioGroup("MaterialZOrigin", z_origin_index) 
    
    
   if  not dialog:ShowDialog() then
      -- DisplayMessageBox("用户取消对话框")
      return false
   end   
   -- if we reach here, user pressed OK on form - get values
   
   --  Directory to process
   g_base_directory   = dialog:GetTextField("DirNameEdit");
   g_process_sub_dirs = dialog:GetCheckBox("ProcessSubDirsCheck")

   --  Output log file
   g_output_log_file  = dialog:GetCheckBox("CreateLogFileCheck")
   g_log_file_path    = g_base_directory .. "\\" .. g_log_file_name

   --
   g_log_file_path2    = g_base_directory .. "\\" .. g_log_file_name2
   
   if g_base_directory == "" then
      MessageBox("必须选择包含DXF文件的目录，此小工具才能工作")
	  return false;
   end
   
   -- Layout Control 
   g_max_items_on_row = dialog:GetIntegerField("NumColumns")
   g_border_gap_x     = dialog:GetDoubleField("BorderGapX")
   g_border_gap_y     = dialog:GetDoubleField("BorderGapY")
   if not job.Exists then
      -- if we reach here we don't have a job currently open so we will create one from the form settings
      g_default_job_width     = dialog:GetDoubleField("DrawingWidth")
      g_default_job_height    = dialog:GetDoubleField("DrawingHeight")
      g_default_job_thickness = dialog:GetDoubleField("DrawingThickness")
      
      units_index = dialog:GetRadioIndex("DrawingUnitsGroup")
      if units_index == 1 then
         g_default_job_in_mm = false
      elseif units_index == 2 then   
         g_default_job_in_mm = true
      else
         MessageBox("来自对话框的未知单位索引 " .. units_index)
      end   
      xy_origin_index = dialog:GetRadioIndex("DrawingOrigin") 
      if xy_origin_index == 1 then
         g_default_job_origin =  "TLC"
      elseif xy_origin_index == 2 then   
         g_default_job_origin =  "TRC"
      elseif xy_origin_index == 3 then   
         g_default_job_origin =  "CENTRE"
      elseif xy_origin_index == 4 then   
         g_default_job_origin =  "BLC"
      elseif xy_origin_index == 5 then   
         g_default_job_origin =  "BRC"
      else
         MessageBox("对话框中未知的XY原点索引 " .. xy_origin_index)
      end
      
      z_origin_index = dialog:GetRadioIndex("MaterialZOrigin") 
      if z_origin_index == 1 then
         g_default_z_on_surface = true
      elseif z_origin_index == 2 then
         g_default_z_on_surface = false
      else
         MessageBox("对话框中未知的Z原点索引 " .. z_origin_index)
      end
      
      -- save job settings as default for next time ....
      registry:SetDouble("DefaultJobWidth",      g_default_job_width)
      registry:SetDouble("DefaultJobHeight",     g_default_job_height)
      registry:SetDouble("DefaultJobThickness",  g_default_job_thickness)
      registry:SetBool  ("DefaultJobInMM",       g_default_job_in_mm)
      registry:SetString("DefaultJobOrigin",     g_default_job_origin)
      registry:SetBool  ("DefaultJobZOnSurface", g_default_z_on_surface)
      end
   
   -- save layout settings as defaults for next time ....
   registry:SetString("BaseDirectory",  g_base_directory)
   registry:SetBool  ("ProcessSubDirs", g_process_sub_dirs)
   registry:SetBool  ("OutputLogFile",  g_output_log_file)

    -- log_file_path = "c:\\temp\\lua_filelist.txt"

   -- ---------------- Variable used to lay out imported vectors -------------------
   registry:SetInt   ("MaxItemsOnRow",      g_max_items_on_row)
   registry:SetDouble("DefaultBorderGapX",  g_border_gap_x)
   registry:SetDouble("DefaultBorderGapY",  g_border_gap_y)
   
   return true
end
--[[ ----------- main -------------------------------------------------
|
| Process all files in 'base_directory' which match 'file_filter'
|
|
]]
function main(script_path)
    -- display form requestiing options from user
    local job = VectricJob()

    if not GetUserChoices(job, script_path) then
       return false
    end

    if not job.Exists then
       DisplayMessageBox("未加载作业-创建默认作业")
       if not CreateJob(
                       g_default_job_name,  
                       g_default_job_width,
                       g_default_job_height, 
                       g_default_job_thickness, 
                       g_default_job_in_mm,
                       g_default_job_origin, 
                       g_default_z_on_surface
                       ) then
          DisplayMessageBox("创建新图形失败")
          return false
       else
          -- DisplayMessageBox("创建新作业")
          job = VectricJob()
       end       
    end
    
    --save path
    local strTempsvg ="mkdir ".. g_base_directory .. "\\svg\\"
    if file_existsA(strTempsvg) == false then
       os.execute(strTempsvg) --windows
    end
    g_sheet_indexi = 1
    g_job_bounds = job:GetBounds()
    
    -- initialise bounding box for row to bottom of page
    g_cur_row_bounds:Merge(g_job_bounds.BLC)

    -- we write a list of files processed to a log file  
    
	if g_output_log_file then
       g_log_file = io.open(g_log_file_path, "w")-- old log
      --creat file rename bat  dxf
       g_log_file2 = io.open(g_log_file_path2, "w") --bat

       g_log_file3 = io.open(g_log_file_path3, "w") --dxf

       if g_log_file ~= nil then
	      g_log_file:write("DXF批处理\n\nFiles ...\n")
	   else
	      DisplayMessageBox("无法创建日志文件\r\n\r\n" .. g_log_file_path)
          g_output_log_file = false
	   end 	
      if g_log_file2 ~= nil then
	   else
	      DisplayMessageBox("无法创建日志文件\r\n\r\n" .. g_log_file_path2)
          g_output_log_file2 = false
	   end 	
      if g_log_file3 ~= nil then
	   else
	      DisplayMessageBox("无法创建日志文件\r\n\r\n" .. g_log_file_path3)
          g_output_log_file3 = false
	   end 	
	end	
    local number_of_files = DirectoryProcessor(
	                                          job,
                                              g_base_directory, 
                                              g_file_filter, 
                                              g_process_sub_dirs, 
                                              ImportDxfFile
                                              )
    if g_output_log_file then
       g_log_file:write("文件数 = " .. number_of_files .. "\r\n")
	   g_log_file:close()
      g_log_file2:close()
      g_log_file3:close()
    end
    MessageBox(
              "处理的文件数 = " .. number_of_files .. "\n" ..
              "\n" 
              )  
    return true    
end
g_DialogHtml = [[
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>DXF批处理器</title>
<style type="text/css">
html {
	overflow: auto;
}
body {
	background-color: #efefef;
}
body, td, th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.FormButton {
	font-weight: bold;
	width: 100%;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.h1 {
	font-size: 14px;
	font-weight: bold;
}
.h2 {
	font-size: 12px;
	font-weight: bold;
}
.ToolNameLabel {
	color: #555;
}
</style>
</head>

<body bgcolor="#efefef">

<table align="center" width="580" bgcolor="#efefef">
  <tr align="center">
    <td align="center">
	
    
 </table>
 

<table align="center" width="95%" border="0">

  
  <tr><td colspan="3"><hr size="1" width="100%" /></td></tr> 

</table>

<table align="center" width="95%" border="0">
    <tr>
      <td colspan="3"><span class="DirectoryPicker">选择要处理的目录: </span></td>
    </tr>
  <tr>
    <td height="30" colspan="3">&nbsp;&nbsp;
    <input name="DirNameEdit" type="text" id="DirNameEdit" size="55" maxlength="128">&nbsp;&nbsp;
    <input type="button" name="DirChooseButton"  id="DirChooseButton" value="选择文件夹 ..." CLASS="DirectoryPicker">
  </tr>
  <tr>
    <td colspan="3">
	    <table width="100%" border="0">
			<tr>
				<td width = "50%">
				&nbsp;&nbsp;<input type="checkbox" name="ProcessSubDirsCheck" id="ProcessSubDirsCheck" value="checkbox">
			   
				<span class="style1"> 处理子目录 </span>
				</td>
				<td width = "50%">
				&nbsp;&nbsp;<input type="checkbox" name="CreateLogFileCheck" id="CreateLogFileCheck" value="checkbox">
			   
				<span class="style1"> 创建日志文件 </span>
				</td>
		
			</tr>
		</table>	
	</td>

  </tr>
  <tr>
    <td colspan="3"><table width="471" border="0">
      <tr>
        <td colspan="2"><span class="DirectoryPicker">布局控制</span></td>
        <td width="145">&nbsp;</td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="302"><span class="style1">连续排列图纸数 </span></td>
        <td><input name="NumColumns" type="text" id="NumColumns" size="4" maxlength="4"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><span class="style1">图纸X之间的间隙 </span></td>
        <td><input name="BorderGapX" type="text" id="BorderGapX" size="8" maxlength="8"> <span id="Units1">inches</span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>图纸Y之间的间隙 </td>
        <td><input name="BorderGapY" type="text" id="BorderGapY" size="8" maxlength="8"> <span id="Units2">inches</span></td>
      </tr>
    </table></td>
  </tr>

  <tr><td colspan="3"><hr size="1" width="100%" /></td></tr> 
  <tr>
   
  </tr>
  <tr>
    <td colspan=3><strong>图纸尺寸 </strong></td>
  </tr>
  <tr>
    <td colspan=3>
		<table width="100%" border="0">
		  <tr>
			<td>&nbsp;&nbsp;图纸宽度（X）</td>
			<td colspan="2"><input name="DrawingWidth" type="text" id="DrawingWidth" size="8" maxlength="8">
			  <span id="Units3">inches</span></td>
		  </tr>
		  <tr>
			<td>&nbsp;&nbsp;图纸高度（Y） </td>
			<td colspan="2"><input name="DrawingHeight" type="text" id="DrawingHeight" size="8" maxlength="8">
			  <span id="Units4">inches</span></td>
		  </tr>
		  <tr>
			<td>&nbsp;&nbsp;图纸厚度（Z）</td>
			<td colspan="2"><input name="DrawingThickness" type="text" id="DrawingThickness" size="8" maxlength="8">
			  <span id="Units5">inches</span></td>
		  </tr>
        </table>
	</td>
  </tr>
  
  <tr>
    <td colspan=3>
	<table width="100%" border="0">
      <tr>
        <td width="20%">&nbsp;&nbsp;单位</td>
        <td width="80%">
		<table width="200">
      
            <td><label>
              <input type="radio" name="DrawingUnitsGroup" value="radio">
              inches</label></td>
        
            <td><label>
              <input type="radio" name="DrawingUnitsGroup" value="radio">
              mm</label></td>
      
        </table>
		</td>
      </tr>
    </table>
	</td>
    </tr>
  <tr>
    <td><strong>XY绘图原点</strong>&nbsp;</td>
	  <td>
      <table width="150" border="0" align="left">
        <tr>
          <td><div align="center">
              <input type="radio" name="DrawingOrigin">
          </div></td>
          <td><div align="center"><strong>----------------</strong></div></td>
          <td valign="top"><div align="center">
              <input type="radio" name="DrawingOrigin">
          </div></td>
        </tr>
        <tr>
          <td><div align="center"><strong>|</strong></div></td>
          <td><div align="center">
              <input type="radio" name="DrawingOrigin">
          </div></td>
          <td valign="top"><div align="center"><strong>|</strong></div></td>
        </tr>
        <tr>
          <td><div align="center">
              <input type="radio" name="DrawingOrigin">
          </div></td>
          <td><div align="center"><strong>----------------</strong></div></td>
          <td valign="top"><div align="center">
              <input type="radio" name="DrawingOrigin">
          </div></td>
        </tr>
      </table></td>
  </tr>

  <tr>
    <td ><strong>Z 起点&nbsp;</strong></td>
      <td>	
      <table width="298" align="left" border=0>
        <tr>
          <td><label>
            <input type="radio" name="MaterialZOrigin" value="radio">
             Z原点在材料上（顶部）</label></td>
        </tr>
        <tr>
          <td><label>
            <input type="radio" name="MaterialZOrigin" value="radio">
           Z原点在材料下（底部）</label></td>
        </tr>
    </table></td>
  </tr>
  <tr><td colspan="3"><hr size="1" width="100%" /></td></tr> 
    <tr><td colspan="3"><table width="100%" border="0">
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="20%"><input name="ButtonOK" type="button" class="FormButton" id="ButtonOK" value="OK"></td>
        <td width="20%">&nbsp;</td>
        <td width="20%"><input name="ButtonCancel" type="button" class="FormButton" id="ButtonCancel" value="取消"></td>
        <td width="20%">&nbsp;</td>
      </tr>
    </table></td></tr> 

</table>

</body>
</html>

]]